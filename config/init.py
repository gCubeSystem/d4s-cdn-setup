from functools import cached_property
from http.cookies import SimpleCookie
from urllib import request
import subprocess
import os
import json

if __name__ == "__main__":

    # intiialize head of branch
    subprocess.run(["/opt/update-head.sh"])   
    
    # pull and initialize all releases
    api_url_parts = os.environ["GIT_SERVER"].split("/")
    api_url_parts.insert(-1, "api/v1/repos")
    api_url_parts.append(os.environ["GIT_REPOSITORY"] + "/releases")
    api_url = "/".join(api_url_parts)
    releases = []
    with request.urlopen(api_url) as data:
        releases = json.loads(data.read())
    
    if releases != None:
        for r in releases:
            if r["target_commitish"] == os.environ["GIT_BRANCH"]:
                subprocess.run(["/opt/update-version.sh", r["tag_name"]])