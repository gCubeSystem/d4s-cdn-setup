# webapp.py

from functools import cached_property
from http.cookies import SimpleCookie
from http.server import BaseHTTPRequestHandler, HTTPServer
from urllib.parse import parse_qsl, urlparse
import subprocess
import os
import json

class WebRequestHandler(BaseHTTPRequestHandler):
    @cached_property
    def url(self):
        return urlparse(self.path)

    @cached_property
    def query_data(self):
        return dict(parse_qsl(self.url.query))

    @cached_property
    def post_data(self):
        content_length = int(self.headers.get("Content-Length", 0))
        return self.rfile.read(content_length)

    @cached_property
    def json_data(self):
        return json.loads(self.post_data.decode("utf-8"))

    @cached_property
    def authorized(self):
        return self.headers.get("Authorization") == os.environ["Authorization"]

    def do_GET(self):
        print(self.url.path)
        if self.url.path == "/health":
            print("returning 200 ok")
            self.send_response(200, "OK")
        else:
            print("returning 404")
            self.send_error(404, "Not found")

    def do_POST(self):
        if not self.authorized:
            self.send_error(401, "Not authorized")
            return

        data = self.json_data
        ret = None
        message = None
        if (data.get("action") == "published" or data.get("action") == "updated") and data.get("release") and data.get("repository"):
            if(data["repository"]["name"] == os.environ["GIT_REPOSITORY"]):
                # Must be a new release of a specific version
                ret = subprocess.run(["/opt/update-version.sh", data["release"]["tag_name"]])
                message = "Updating version " + data["release"]["tag_name"]
            else:
                message = "New release on unknown repository"
        elif data.get("action") == "deleted" and data.get("release") and data.get("repository"):
            if(data["repository"]["name"] == os.environ["GIT_REPOSITORY"]):
                # Must be a deleteion of a release
                ret = subprocess.run(["/opt/delete-version.sh", data["release"]["tag_name"]])
                message = "Deleting release"
            else:
                message = "Deleted release on unknown repository"
        elif data.get("action") == "closed" and data.get("pull_request"):
            pr = data.get("pull_request")
            if pr["merged"] and data["repository"]["name"] == os.environ["GIT_REPOSITORY"] and pr["base"]["ref"] == os.environ["GIT_BRANCH"]:
                # Must be a pull request onto the declared branch
                ret = subprocess.run(["/opt/update-head.sh"])
                message = "Updating head after PR"
            else:
                message = "Closed PR on unknown repository"
        elif data.get("commits") and data.get("pusher") and data.get("repository") and data.get("ref"):
            if(data["repository"]["name"] == os.environ["GIT_REPOSITORY"] and data.get("ref") == "refs/heads/" + os.environ["GIT_BRANCH"]):
                # Must be a push to the branch
                ret = subprocess.run(["/opt/update-head.sh"])
                message = "Updating head after push"
            else:
                message = "Push onto unknown branch or repository"
        else:
            message = "Unknwown operation"

        if ret != None and ret.returncode == 0:
            self.send_response(200, message)
        else:
            self.send_error(500, message)
        
        self.send_header("Content-Type", "text/plain")
        self.end_headers()
        self.wfile.write(message.encode("UTF-8"))
        self.wfile.flush()

if __name__ == "__main__":   
    server = HTTPServer(("0.0.0.0", 8000), WebRequestHandler)
    server.serve_forever()