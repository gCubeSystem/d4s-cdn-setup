#!/bin/bash

# EXAMPLES
#GIT_BRANCH=master
#GIT_REPOSITORY=web-components

curl $GIT_SERVER/$GIT_REPOSITORY/archive/$GIT_BRANCH.tar.gz -o /var/www/$GIT_BRANCH.tgz
tar xzf /var/www/$GIT_BRANCH.tgz -C /var/www
rm -rf /var/www/html/head
mv /var/www/$GIT_REPOSITORY /var/www/html/head
rm -rf /var/www/$GIT_BRANCH.tgz