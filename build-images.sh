#!/usr/bin/env bash
if [ $# -eq 0 ]
  then
    tag='dev'
  else
    tag=$1
fi

docker build -t hub.dev.d4science.org/cdn/cdn:$tag .
docker push hub.dev.d4science.org/cdn/cdn:$tag