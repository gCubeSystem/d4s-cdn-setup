FROM python:3.9.18-slim-bullseye
RUN apt-get update && apt-get install -y nginx curl
RUN mkdir -p /var/www/html && chown -R www-data: /var/www
COPY config/*.sh /opt/
RUN chmod 755 /opt/update-head.sh /opt/update-version.sh /opt/delete-version.sh
COPY config/default.conf /etc/nginx/conf.d/default.conf
COPY config/*.py /opt/
COPY config/docker-entrypoint.sh /docker-entrypoint.sh
RUN chmod 755 /docker-entrypoint.sh && chown -R www-data: /docker-entrypoint.sh

RUN set -x \
# create nginx user/group first, to be consistent throughout docker variants
    && groupadd --system --gid 101 nginx \
    && useradd --system --gid nginx --no-create-home --home /nonexistent --comment "nginx user" --shell /bin/false --uid 101 nginx \
    && apt-get remove --purge --auto-remove -y && rm -rf /var/lib/apt/lists/* /etc/apt/sources.list.d/nginx.list

# forward request and error logs to docker log collector
RUN ln -sf /dev/stdout /var/log/nginx/access.log && ln -sf /dev/stderr /var/log/nginx/error.log

CMD ["/docker-entrypoint.sh"]
